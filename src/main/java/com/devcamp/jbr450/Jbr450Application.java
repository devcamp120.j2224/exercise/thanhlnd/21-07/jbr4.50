package com.devcamp.jbr450;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr450Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr450Application.class, args);
	}

}
