package com.devcamp.jbr450;

public class Customer {
    int id;
    String name;
    int discount;


    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getDiscount() {
        return discount;
    }


    public void setDiscount(int discount) {
        this.discount = discount;
    }


    @Override
    public String toString() {
        return String.format("Name (id=%s), (discount=%s %)", id, discount);
    }

 
    
}
