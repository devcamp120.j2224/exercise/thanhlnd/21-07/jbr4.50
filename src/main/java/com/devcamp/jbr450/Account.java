package com.devcamp.jbr450;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;

    
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }


    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public Customer getCustomer() {
        return customer;
    }


    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    public double getBalance() {
        return balance;
    }


    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName(){
        return this.customer.name;
    }

    public void deposit(double amount){
        balance = balance + amount;
    }

    public void withdraw(double amount){
        if(balance >= amount){
            balance = balance - amount;
        }else{
            System.out.println("amount withdrawn exceeds the current balance!");
        }
    }

    @Override
    public String toString() {
        return String.format("%s (ID = %s), balance = %s,", this.customer.name,id,balance);
    }

    
}
