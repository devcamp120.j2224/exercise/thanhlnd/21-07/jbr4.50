package com.devcamp.jbr450;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerAccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> ListAccount() {
        Customer customer1 = new Customer(1, "Bao", 10);
        Customer customer2 = new Customer(1, "Thanh", 20);
        Customer customer3 = new Customer(1, "Thiên", 30);

        // System.out.println(customer1.toString());
        // System.out.println(customer2.toString());
        // System.out.println(customer3.toString());

        Account account1 = new Account(101, customer1);
        Account account2 = new Account(102, customer2, 10000);
        Account account3 = new Account(103, customer3, 20000);

        // System.out.println(account1.toString());
        // System.out.println(account2.toString());
        // System.out.println(account3.toString());

        ArrayList<Account> accounts = new ArrayList<>();

        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        return accounts;
    }
}
